package nl.jackberg.springrest.message;

import nl.jackberg.springrest.message.dto.CreateMessageDTO;
import nl.jackberg.springrest.message.dto.UpdateMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    List<MessageEntity> getMessages() {
        return (List<MessageEntity>) messageRepository.findAll();
    }

    void createMessage(CreateMessageDTO dto) {
        MessageEntity entity = new MessageEntity();
        entity.setMessage(dto.getMessage());
        messageRepository.save(entity);
    }

    void updateMessage(long id, UpdateMessageDTO dto) {
        Optional<MessageEntity> optionalEntity = messageRepository.findById(id);
        if (optionalEntity.isPresent()) {
            MessageEntity entity = optionalEntity.get();
            entity.setMessage(dto.getMessage());
            messageRepository.save(entity);
        }
    }

    void deleteMessage(long id) {
        messageRepository.deleteById(id);
    }
}
