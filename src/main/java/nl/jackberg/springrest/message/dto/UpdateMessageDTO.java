package nl.jackberg.springrest.message.dto;

public class UpdateMessageDTO {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
