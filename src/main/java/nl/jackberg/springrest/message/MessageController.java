package nl.jackberg.springrest.message;

import nl.jackberg.springrest.message.dto.CreateMessageDTO;
import nl.jackberg.springrest.message.dto.GetMessageDTO;
import nl.jackberg.springrest.message.dto.UpdateMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/messages")
    public List<GetMessageDTO> getMessages() {
        List<MessageEntity> messages = messageService.getMessages();

        return messages.stream()
                .map(entity -> new GetMessageDTO(entity.getId(), entity.getMessage()))
                .collect(Collectors.toList());
    }

    @PostMapping("/messages")
    public ResponseEntity<?> createMessage(@RequestBody CreateMessageDTO dto) {
        messageService.createMessage(dto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/messages/{id}")
    public ResponseEntity<?> updateMessage(
            @RequestBody UpdateMessageDTO dto,
            @PathVariable long id
    ) {
        messageService.updateMessage(id, dto);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/messages/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable long id) {
        messageService.deleteMessage(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
